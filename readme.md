#FindMyI

###Version 0.2

Requires [requests](https://github.com/kennethreitz/requests)

**findmyi** is an simple python library to search your iOS device over iClouds Find my iPhone Service.

## Features

* Get current Device Location
* Support for Sending Messages
* Support for remote locking your device
* Remote delete your device

## Simple Usage

this is just a basic example:

```
from findmyi import FMI

finder = FMI( "yourmail@me.com", "PASSWORD" )
finder.updateDevice()

for deviceNum in range(len(finder.devices)):
    print finder.devices[deviceNum]
```

now you can call the list of devices by useing `finder.devices`

each device gives you an dictonary with some basic informations and /if supportet/ an latitude/longitude fiedl like this

```
{

    'displayName': u'iPad',
    'name': u'Huntsman',
    'battery': 0.0,
    'model': u'ThirdGen-white',
    'id': u'DEVICEIDGOESHERe',
    'latitude': 52.1348086086,
    'longitude': 6.20540518,
    'accuracy': 100.0,
    'positionType': u'Wifi',
    'updateTime': 1375958135394
}
```

## updating the device list

```
finder.updateDevice()
```

## update just the location of a device

If you just want to monitor the location of one single device without haveing to update all others the whole time you can use the `locate` function.

```
finder.locate( deviceNum )
```

this returns a location dictonary for the selected device

## Sending a Message

Sending Messages is really easy. Use the deviceNum from your finder.devices list above.

```
finder.sendMessage( deviceNum, "TITLE", "MESSAGE", sound=True )
```


## Lock your Device

just use the deviceNum and any 4 digit code which must be entered to unlock the device.

```
finder.lockDevice( deviceNum, UNLOCKCODE )
```

## Remote Delete your Device

**Caution** Use with care, this whipes your device completely and unreversable.

```
finder.whipeDevice( deviceNum )
```
